create schema dw;

drop table lance;
drop table lote;
drop table produto;
drop table cliente;
drop table leilao;
drop table tipo_produto;


create table tipo_produto(
	id_tipo_produto SERIAL,
	nome_tipo_produto VARCHAR(100) UNIQUE,
	
	CONSTRAINT pk_id_tipo_produto PRIMARY KEY (id_tipo_produto)
);

create table produto(
	id_produto SERIAL,
	nome_produto VARCHAR(100),
	id_tipo_produto INTEGER,
	valor_produto NUMERIC(15,2),
	data_registro_produto timestamp,
		
	CONSTRAINT pk_id_produto PRIMARY KEY (id_produto),
	CONSTRAINT fk_produto_tipo_produto FOREIGN KEY  (id_tipo_produto) REFERENCES tipo_produto(id_tipo_produto)
	
);

create table leilao(
	id_leilao SERIAL,
	nome_leilao VARCHAR(1000),
	descricao VARCHAR(1000),
	data_realizacao TIMESTAMP,
	
	CONSTRAINT pk_leilao PRIMARY KEY (id_leilao)

);

create table cliente(
	id_cliente SERIAL,
	nome_cliente VARCHAR(200),
	identificacao_fiscal VARCHAR(25),
	
	CONSTRAINT pk_id_cliente PRIMARY KEY (id_cliente)
);

create table lote(
	id_lote SERIAL,
	nome_lote VARCHAR(100),
	preco_inicial NUMERIC(15,2),
	id_leilao INTEGER,
	id_produto INTEGER,
	
	CONSTRAINT pk_lote PRIMARY KEY (id_lote),
	CONSTRAINT fk_lote_leilao FOREIGN KEY  (id_leilao) REFERENCES leilao(id_leilao),
	CONSTRAINT fk_lote_produto FOREIGN KEY  (id_produto) REFERENCES produto(id_produto)
	
);

create table lance(
	id_lance SERIAL,
	valor NUMERIC(15,2),
	id_lote INTEGER,
	id_cliente INTEGER,
	ind_ultimo_lance BOOLEAN,
	
	
	CONSTRAINT pk_lance PRIMARY KEY (id_lance),
	CONSTRAINT fk_lance_lote FOREIGN KEY  (id_lote) REFERENCES lote(id_lote),
	CONSTRAINT fk_lance_cliente FOREIGN KEY  (id_cliente) REFERENCES cliente(id_cliente)

);

