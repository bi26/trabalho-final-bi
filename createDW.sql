create schema dw;

drop table dw.ft_venda;
drop table dw.dim_produto;
drop table dw.dim_cliente;
drop table dw.dim_local;

create table dw.dim_produto(

	sk_produto 				SERIAL,
	nk_produto 				INTEGER,
	nome_produto			VARCHAR(100),
	tipo_produto			VARCHAR(100),
	valor_produto			NUMERIC(15,2),
	data_registo_produto	TIMESTAMP,
	origem					VARCHAR(1),
	
	CONSTRAINT sk_produto PRIMARY KEY (sk_produto)

);

create table dw.dim_cliente(

	sk_cliente 		SERIAL,
	nome_cliente	VARCHAR(1000),
	nk_cliente 		BIGINT,
	tipo_cliente	VARCHAR(2),
	
	CONSTRAINT sk_cliente PRIMARY KEY (sk_cliente)

);

create table dw.dim_local(
	
	sk_local 		SERIAL,
	nome_local		VARCHAR(1000),
	nk_local		INTEGER,
	ind_local		VARCHAR(1),
	
	CONSTRAINT sk_local PRIMARY KEY (sk_local)

);


create table dw.ft_venda(

	fk_local					INTEGER,
	fk_data					INTEGER,
	fk_produto					INTEGER,
	fk_cliente					INTEGER,
	valor_individual_venda		NUMERIC(15,2),
	
	CONSTRAINT fk_local 	FOREIGN KEY  (fk_local) 	REFERENCES dw.dim_local(sk_local),
	CONSTRAINT fk_data 		FOREIGN KEY  (fk_data) 		REFERENCES dw.dim_data(sk_data),
	CONSTRAINT fk_produto 	FOREIGN KEY  (fk_produto) 	REFERENCES dw.dim_produto(sk_produto),
	CONSTRAINT fk_cliente 	FOREIGN KEY  (fk_cliente) 	REFERENCES dw.dim_cliente(sk_cliente)

);

